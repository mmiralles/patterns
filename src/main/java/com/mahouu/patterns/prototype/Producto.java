package com.mahouu.patterns.prototype;

public interface Producto {
    Object clone();
}