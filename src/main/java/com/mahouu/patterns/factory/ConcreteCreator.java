package com.mahouu.patterns.factory;

public class ConcreteCreator extends Creator
{
    @Override
    public ConcreteProduct factoryMethod()
    {
        return new ConcreteProduct();
    }
}
