package com.mahouu.patterns.factory;

public abstract class Creator
{
    // Definimos método abstracto
    public abstract Product factoryMethod();
}
