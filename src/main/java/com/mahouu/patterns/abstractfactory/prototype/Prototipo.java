package com.mahouu.patterns.abstractfactory.prototype;

public interface Prototipo {

        public Prototipo clone();
}