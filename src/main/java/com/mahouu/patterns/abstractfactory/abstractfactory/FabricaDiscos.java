package com.mahouu.patterns.abstractfactory.abstractfactory;

import com.mahouu.patterns.abstractfactory.BluRay;
import com.mahouu.patterns.abstractfactory.DVD;

public interface FabricaDiscos {

        public BluRay crearBluRay();
        public DVD crearDVD();
}
