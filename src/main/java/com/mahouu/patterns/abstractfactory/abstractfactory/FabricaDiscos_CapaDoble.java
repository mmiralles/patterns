package com.mahouu.patterns.abstractfactory.abstractfactory;

import com.mahouu.patterns.abstractfactory.BluRay;
import com.mahouu.patterns.abstractfactory.BluRay_CapaDoble;
import com.mahouu.patterns.abstractfactory.DVD;
import com.mahouu.patterns.abstractfactory.DVD_CapaDoble;


public class FabricaDiscos_CapaDoble implements FabricaDiscos {

        @Override
        public BluRay crearBluRay() {
                return new BluRay_CapaDoble();
        }

        @Override
        public DVD crearDVD() {
                return new DVD_CapaDoble();
        }

}