package com.mahouu.patterns.abstractfactory.abstractfactory;

import com.mahouu.patterns.abstractfactory.BluRay;
import com.mahouu.patterns.abstractfactory.BluRay_CapaSimple;
import com.mahouu.patterns.abstractfactory.DVD;
import com.mahouu.patterns.abstractfactory.DVD_CapaSimple;

public class FabricaDiscos_CapaSimple implements FabricaDiscos {

       @Override
       public BluRay crearBluRay() {
               return new BluRay_CapaSimple();
       }

       @Override
       public DVD crearDVD() {
               return new DVD_CapaSimple();
       }

}