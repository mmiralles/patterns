package com.mahouu.patterns.abstractfactory;

import com.mahouu.patterns.abstractfactory.prototype.*;

public class BluRay_CapaDoble extends BluRay {

    @Override
    public Prototipo clone() {
            return new BluRay_CapaDoble();
    }

    @Override
    public String getCapacidad() {
            return "50GB";
    }

    @Override
    public String getNombre() {
            return "BluRay Capa Doble";
    }

    @Override
    public String getPrecio() {
            return "40.00$";
    }

}