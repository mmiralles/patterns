package com.mahouu.patterns;

import com.mahouu.patterns.prototype.FactoriaPrototipo;
import com.mahouu.patterns.prototype.Producto;
import com.mahouu.threads.ClassFromRunable;
import com.mahouu.threads.SecondClassFromRunable;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class PrototypeTest
{

	@Test
	public void singleThread() {
        FactoriaPrototipo factoria = new FactoriaPrototipo();
        Producto producto = (Producto) factoria.create("producto 1");
	}

}
