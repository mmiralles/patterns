package com.mahouu.patterns;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.mahouu.threads.ClassFromRunable;
import com.mahouu.threads.SecondClassFromRunable;

public class ThreadsTest {
	
	private ClassFromRunable r1;
	private SecondClassFromRunable r2;
	
	@Before
	public void setUp(){
		r1 = new ClassFromRunable();
		r2 = new SecondClassFromRunable();
	}

	@Test
	public void singleThread() {
		System.out.println("single - init");
	    r1.run();
	    r2.run();
		System.out.println("single - finish");
		assertNotNull("Should be not null", r1);
	}
	
	@Test
	public void multipleThreads(){
		Thread t1 = new Thread(r1);
	    Thread t2 = new Thread(r2);
		System.out.println("Multiple - init");
		t1.start();
	    t2.start();
		System.out.println("Multiple - finish");
		assertNotNull("Should be not null", t1);
	}

}
