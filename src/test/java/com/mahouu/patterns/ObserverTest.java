package com.mahouu.patterns;

import junit.framework.Assert;

import org.junit.Test;

import com.mahouu.patterns.observer.Contador;
import com.mahouu.patterns.observer.Detector;
import com.mahouu.patterns.observer.Medidor;
import com.mahouu.patterns.observer.ValorContador;

public class ObserverTest
{

    @Test
    public void observerTest()
    {

        Contador c = new Contador(0, 255);

        Detector d = new Detector();
        c.agregarObservador(d);
        c.incrementarContador(5);

        ValorContador v = new ValorContador(c);
        c.agregarObservador(v);
        c.incrementarContador(5);

        Medidor m = new Medidor(c);
        c.agregarObservador(m);
        c.eliminarObservador(d);
        c.incrementarContador(19);

        Assert.assertEquals("Should be 29", "29", "" + c.valor());
    }
}
