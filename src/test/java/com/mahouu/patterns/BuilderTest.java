package com.mahouu.patterns;

import junit.framework.Assert;

import org.junit.Test;

import com.mahouu.patterns.builder.Cocina;
import com.mahouu.patterns.builder.HawaiPizzaBuilder;
import com.mahouu.patterns.builder.PicantePizzaBuilder;
import com.mahouu.patterns.builder.Pizza;
import com.mahouu.patterns.builder.PizzaBuilder;

public class BuilderTest
{

    @Test
    public void runBuilder()
    {
        Cocina cocina = new Cocina();
        PizzaBuilder hawai_pizzabuilder = new HawaiPizzaBuilder();
        PizzaBuilder picante_pizzabuilder = new PicantePizzaBuilder();

        cocina.setPizzaBuilder(hawai_pizzabuilder);
        cocina.construirPizza();

        Pizza pizza = cocina.getPizza();

        Assert.assertNotNull("Should be not null", pizza);
    }

}
