package com.mahouu.patterns;

import org.junit.Test;

import com.mahouu.patterns.factory.ConcreteCreator;
import com.mahouu.patterns.factory.Creator;
import com.mahouu.patterns.factory.Product;

public class FactoryTest
{

    @Test
    public void runFactory()
    {
        Creator aCreator;
        aCreator = new ConcreteCreator();
        Product producto = aCreator.factoryMethod();
        producto.operacion();
    }

}
