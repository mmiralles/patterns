package com.mahouu.patterns;

import junit.framework.Assert;

import com.mahouu.patterns.abstractfactory.BluRay;
import com.mahouu.patterns.abstractfactory.DVD;
import com.mahouu.patterns.abstractfactory.abstractfactory.FabricaDiscos;
import com.mahouu.patterns.abstractfactory.abstractfactory.FabricaDiscos_CapaDoble;
import com.mahouu.patterns.abstractfactory.abstractfactory.FabricaDiscos_CapaSimple;

import org.junit.Test;

public class AbstractFactoryTest {
	
	@Test
    public void executePattern() {
        FabricaDiscos fabrica;
        DVD dvd;
        BluRay bluray;

        fabrica = new FabricaDiscos_CapaSimple();
        dvd = fabrica.crearDVD();
        bluray = fabrica.crearBluRay();

        System.out.println(dvd);
        System.out.println(bluray);

        fabrica = new FabricaDiscos_CapaDoble();
        dvd = fabrica.crearDVD();
        bluray = fabrica.crearBluRay();

        System.out.println(dvd);
        System.out.println(bluray);
        
        Assert.assertNotNull("Should be not null", dvd);
    }

}
